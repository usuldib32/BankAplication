package Bank.Service;

import Bank.Model.Customer;
import Bank.Repository.UserRepository;

public class UserService {

    private UserRepository repository;

    public UserService() {
        this.repository = new UserRepository();
    }

    public void registerCustomer(Customer customer) {
        this.repository.addCustomer(customer);
    }

    public Customer loginCustormer(String login, String password) {
        Customer customer = repository.getCustomerByLogin(login);
        if (customer != null && customer.getCredentials().getPassword().equals(password)) {
            return customer;
        }
        return null;
    }
}
