package Bank.Repository;

import Bank.Model.Customer;

import java.util.HashSet;
import java.util.Set;

public class UserRepository {

    private Set<Customer> customers = new HashSet<>();

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public Customer getCustomerByLogin(String login) {
        for (Customer customer : customers) {
            if (isCorrectLogin(login, customer)) {
                return customer;
            }
        }
        return null;
    }

    private boolean isCorrectLogin(String login, Customer customer) {
        return customer.getCredentials().getLogin().equals(login)
                || customer.getCredentials().getEmail().equals(login);
    }
}
